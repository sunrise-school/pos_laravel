<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class LanguageController extends Controller
{
    public function switchLanguage($lang){
        App::setlocale($lang);
        session()->put('locale', $lang);
        // session()->forget('locale');

        return redirect()->back();
    }
}
