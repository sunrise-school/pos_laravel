<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTable;
use DB;

class VideoController extends Controller
{
    public function index(Request $r){
        if($r->ajax()){
            $data = DB::table('videos')->where('active', 1);

            return DataTable::of($data)
                            ->addIndexColumn()
                            ->addColumn('action', function($row){
                                $edit =  '<button type="button" btnedit="' . $row->id . '" tbl="videos"  class="btn btn-sm btn-success" onclick="edit(this)"> <i class="fa fa-edit"></i> </button>';
                                $delete = '<button type="button" btndelete="'. $row->id .'" tbl="videos" class="btn btn-sm btn-danger" onclick="deleteData(this)" > <i class="fa fa-trash"></i> </button>';

                                $btn = $delete .' '. $edit;
                                return $btn;
                            })
                            ->addColumn('checkbox', function($row){
                                $checkbox = '<input type="checkbox" eid="'. $row->id .'" />';
                                return $checkbox;
                            })

            ->rawColumns(['action', 'checkbox'])
            ->make(true);
        }

        return view('backend.videos.index');
    }
}
