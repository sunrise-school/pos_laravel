<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTable;
use DB;

class RoleController extends Controller
{
    public function index(Request $r){
        if($r->ajax()){
            $data = DB::table('roles')->where('active', 1);
    
            return DataTable::of($data)
                            ->addIndexColumn()
                            ->addColumn('action', function($row){
                                $view =  '<a href="'. route('admin.role_permission',$row->id) .'" class="btn btn-primary btn-sm mr-2">View Role Permission</a>';
                                $edit =  '<button type="button" btnedit="' . $row->id . '" tbl="roles"  class="btn btn-sm btn-success" onclick="edit(this)"> <i class="fa fa-edit"></i> </button>';
                                $delete = '<button type="button" btndelete="'. $row->id .'" class="btn btn-sm btn-danger" onclick="deleteRole(this)" > <i class="fa fa-trash"></i> </button>';
    
                                $btn = $view . $delete .' '. $edit;
                                return $btn;
                            })
    
            ->rawColumns(['action'])
            ->make(true);
        }
    
        return view('backend.roles.index');
    }
    public function insert(Request $r){
        if($r->ajax()){
            $data = $r->except("_token");

            DB::beginTransaction();
            try {

                $role_id = DB::table("roles")->insertGetId($data);

                $permissions = DB::table('permissions')->where('active',1)->get();

                foreach($permissions as $permission){
                    DB::table("role_permissions")->insert([
                        'role_id' => $role_id,
                        'permission_id' => $permission->id,
                        'insert' => 1,
                        'list' => 1,
                        'update' => 1,
                        'delete' => 1
                    ]);
                }

                DB::commit();

                return response()->json(['status' => 'success', 'message' => 'insert successfully.']);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
            }
        }
    }
    public function delete(Request $r){
        if($r->ajax()){
            DB::beginTransaction();
            try {
                
                DB::table("roles")->where('id',$r->eid)->update(['active' => 0]);

                $role_permissions = DB::table('role_permissions')->where('role_id',$r->eid)->get();

                // foreach($role_permissions as $role_permision){
                //     DB::table('role_permissions')->where('id',$role_permision->id)->update(['active' => 0]);
                // }

                DB::commit();
                return response()->json(['status' => 'success', 'message' => 'delete successfully.']);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['status' => 'error', 'message' => $e->getMessage()]);
            }
        }
    }
}
