<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $r){
        if(session()->has('user')){
            return redirect()->route('dashboard');
        }
        if($r->_token){
            $user = DB::table('users')->where([
                'username' => $r->username,
            ])->first();

            if($user){
                if($user->active != 1){
                    return redirect()
                        ->back()
                        ->withInput()
                        ->with('error','Username not found');
                }
                if (Hash::check($r->password ,$user->password)) {
                    session()->put('user',$user);
                    return redirect()->route('dashboard')->with('success','Login Successfully.');
                } else{
                    return redirect()
                        ->back()
                        ->withInput()
                        ->with('error','username or password is incorrect !');
                }
            } else {
                return redirect()
                        ->back()
                        ->withInput()
                        ->with('error','username or password is incorrect !');
            }
        }
        return view('backend.auth.login');
    }
    public function logout(){
        if(session()->has('user')){
            session()->forget('user');
        }
        return redirect()->route('auth.login')->with('success','Logout Successfully.');
    }
}
