<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function index(){
        $data['user'] = DB::table("users")->find(session()->get('user')->id);
        return view('backend.profile.index',$data);
    }
    public function update(Request $r){
        $data = $r->except('_token','photo','password');
        if($r->photo){
            $data['photo'] = $r->photo->store('/image','custom');
        }
        $data['password'] = Hash::make($r->password);

        $update = DB::table('users')->where('id',session()->get('user')->id)->update($data);
        if($update){
            return redirect()->back()->with('success','update successfully!');
        } else {
            return redirect()->back()->with('error','update fails!');
        }
    }
}
