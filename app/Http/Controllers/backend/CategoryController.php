<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTable;

class CategoryController extends Controller
{
    public function index(Request $r){
        if($r->ajax()){
            $data = DB::table('categories')->where('active', 1);

            return DataTable::of($data)
                            ->addIndexColumn()
                            ->addColumn('action', function($row){
                                $edit =  '<button type="button" btnedit="' . $row->id . '" tbl="categories"  class="btn btn-sm btn-success" onclick="edit(this)"> <i class="fa fa-edit"></i> </button>';
                                $delete = '<button type="button" btndelete="'. $row->id .'" tbl="categories" class="btn btn-sm btn-danger" onclick="deleteData(this)" > <i class="fa fa-trash"></i> </button>';

                                $btn = $delete .' '. $edit;
                                return $btn;
                            })
                            ->addColumn('checkbox', function($row){
                                $checkbox = '<input type="checkbox" eid="'. $row->id .'" />';
                                return $checkbox;
                            })
                            ->addColumn('photo', function($row){
                                $photo = '<img src="' . url('/') . '/' . $row->photo .  '" width="24" class="rounded-circle">';
                                return $photo;
                            })

            ->rawColumns(['action', 'photo', 'checkbox'])
            ->make(true);
        }

        return view('backend.categories.index');
    }
}
