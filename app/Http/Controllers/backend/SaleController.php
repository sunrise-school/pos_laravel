<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class SaleController extends Controller
{
    public function index(){
        $data['products'] = DB::table('products')->where('active',1)->get();
        $data['methods'] = DB::table('payment_methods')->where('active',1)->get();

        return view('backend.sales.index',$data);
    }
    public function insert(Request $r){

        DB::beginTransaction();
        try { 
            $items = $r->items;
            $totalPrice = $r->amount;
            $method = $r->payment;

            $inv_id = DB::table('invoices')
                        ->insertGetId([
                            'code' => sprintf("%04d", DB::table("invoices")->count() + 1),
                            'payment_method_id' => $method["id"],
                            'payment_method_name' => $method["name"],
                            'total_price' => $totalPrice,
                            'sale_by' => session()->get('user')->id,
                            'created_by' => session()->get('user')->id,
                        ]);
            
            foreach($items as $i){
                DB::table('invoice_details')->insert([
                    'invoice_id' => $inv_id,
                    'product_id' => $i["id"],
                    'product_name' => $i["name"],
                    'product_original_price' => $i["original_price"],
                    'product_price' => $i["price"],
                    'qty' => $i["qty"],
                    'total_price' => $i["qty"] * $i["price"],
                    'created_by' => session()->get('user')->id
                ]);
            }

            DB::commit();

            return response()->json(['status' => "success", 'id' => $inv_id, 'message' => 'insert successfully!']);

        } catch (\Exception $ex) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $ex->getMessage()], 200);
        }
    }
    public function print($id){

        $data['company'] = DB::table("companies")->find(1);
        $data['invoice'] = DB::table("invoices")->find($id);
        $data['sale_by'] = DB::table("users")->find($data['invoice']->sale_by)->name;
        $data['invoice_details'] = DB::table("invoice_details")->where("invoice_id",$id)->get();

        return view('backend.prints.index', $data);
    }
}
