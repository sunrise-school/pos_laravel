<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTable;

class UserController extends Controller
{
    public function index(Request $r){
        
        // if($r->ajax()){
        //     $user = DB::table("users")->where('active',1)->get();
        //     return response()->json(['user' => $user], 200);
        // }

        if($r->ajax()){
            $data = DB::table('users')
                    ->join('roles','roles.id','users.role_id')
                    ->select("users.*",'roles.name as role_name')
                    ->where('users.active', 1);

            return DataTable::of($data)
                            ->addIndexColumn()
                            ->addColumn('action', function($row){
                                $edit =  '<button type="button" btnedit="' . $row->id . '" class="btn btn-sm btn-success" onclick="edit(this)"> <i class="fa fa-edit"></i> </button>';
                                $delete = '<button type="button" btndelete="'. $row->id .'" class="btn btn-sm btn-danger" onclick="deleteUser(this)" > <i class="fa fa-trash"></i> </button>';

                                $btn = $delete .' '. $edit;
                                return $btn;
                            })
                            ->addColumn('photo', function($row){
                                $photo = '<img src="' . url('/') . '/' . $row->photo .  '" width="24" class="rounded-circle">';
                                return $photo;
                            })

            ->rawColumns(['action', 'photo'])
            ->make(true);
        }

        $data['roles'] = DB::table("roles")->where('active',1)->get();


        return view('backend.users.index', $data);
    }
    public function edit($id){
        $userDetail = DB::table('users')->find($id);
        return response()->json(['userDetail' => $userDetail],200);
    }
    public function update(Request $r, $id){
        $update = DB::table("users")->where('id',$id)
                ->update([
                    'username' => $r->username,
                    'password' => $r->password,
                    'role_id' => $r->role_id
                ]);
        
        if($update){
            $sms = "<div class='alert alert-success'>Update success</div>";
            return response()->json(['status' => 'success', 'message' => $sms ], 200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'update success'], 200);
        }
    }
    public function create(Request $r){
        $data = $r->all();
        $data['email'] = date('H:i:s') . '@gamil.com';
        $createGetId = DB::table("users")->insertGetId([
            'name' => $r->name,
            'username' => $r->username,
            'password' => $r->password,
            'role_id' => $r->role_id,
            'email' => time() .'@gamil.com'
        ]);
        if(true){
            return response()->json(['status' => 'success', 'message' => 'Insert successfully'],200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'insert fail'], 200);
        }
    }
    public function delete(Request $r){
        $d = DB::table('users')->where('id',$r->id)->update(['active' => 0]);

        if($d){
            return response()->json(['status' => 'success', 'message' => 'Delete Successfully'],200);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Delete Fail'],200);
        }
    }
}
