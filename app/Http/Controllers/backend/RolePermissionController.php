<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTable;
use DB;

class RolePermissionController extends Controller
{
    public function index(Request $r, $role_id){
        if($r->ajax()){
            $data = DB::table('role_permissions')
                    ->join('permissions','permissions.id','role_permissions.permission_id')
                    ->where('role_permissions.role_id', $role_id)
                    ->where('role_permissions.active', 1)
                    ->select("role_permissions.*",'permissions.alias as permission_name');
    
            return DataTable::of($data)
                            ->addIndexColumn()
                            ->addColumn('action', function($row){
                                return '';
                            })
    
            ->rawColumns(['action'])
            ->make(true);
        }
        $data['role_id'] = $role_id;
        return view('backend.role_permissions.index',$data);
    }
    public function update(Request $r){
        if($r->ajax()){
            $id = $r->id;
            $column = $r->col;
            $value = $r->value == 1 ? 0 : 1;

            $update = DB::table("role_permissions")
                        ->where('id',$id)
                        ->update(["$column" => "$value", 'updated_by' => session()->get('user')->id]);
            if($update){
                return response()->json(['status' => 'success', 'message' => "update successfully"], 200);
            } else {
                return response()->json(['status' => 'error', 'message' => "update fail"], 200);
            }
        }
    }
    public function generate($role_id){
        $getPermissions = DB::table('permissions')->where('active',1)->get();

        $getRolePermission = DB::table('role_permissions')->where('role_id',$role_id)->where('active',1)->get();

        foreach($getPermissions as $per){
            if(count($getPermissions) == 0){
                DB::table('role_permissions')->insert([
                    'role_id' => $role_id,
                    'permission_id' => $per->id,
                    'list' => 1,
                    'update' => 1,
                    'insert' => 1,
                    'delete' => 1
                ]);
            } else {
                $check = DB::table('role_permissions')->where('role_id',$role_id)->where('permission_id',$per->id)->first();
                if(!$check){
                    DB::table('role_permissions')->insert([
                        'role_id' => $role_id,
                        'permission_id' => $per->id,
                        'list' => 1,
                        'update' => 1,
                        'insert' => 1,
                        'delete' => 1
                    ]);
                }
            }
        }

        return redirect()->back();
    }
}
