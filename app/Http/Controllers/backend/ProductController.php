<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTable;
use DB;

class ProductController extends Controller
{
    public function index(Request $r){
        if(checkPermission('product','list') != 1){
            return redirect()->route('404');
        }
        if($r->ajax()){
            $data = DB::table('products')
                ->join('categories','categories.id','products.category_id')
                ->select('products.*','categories.name as category_name')
                ->where('products.active', 1);

            return DataTable::of($data)
                            ->addIndexColumn()
                            ->addColumn('action', function($row){
                                $edit =  '<button type="button" btnedit="' . $row->id . '" tbl="products"  class="btn btn-sm btn-success" onclick="edit(this)"> <i class="fa fa-edit"></i> </button>';
                                $delete = '<button type="button" btndelete="'. $row->id .'" tbl="products" class="btn btn-sm btn-danger" onclick="deleteData(this)" > <i class="fa fa-trash"></i> </button>';

                                $btn = '';
                                if(checkPermission('product','update') == 1){
                                    $btn .= $edit;
                                }
                                if(checkPermission('product','delete') == 1){
                                    $btn .= $delete;
                                }

                                return $btn;
                            })
                            ->addColumn('checkbox', function($row){
                                $checkbox = '<input type="checkbox" eid="'. $row->id .'" />';
                                return $checkbox;
                            })
                            ->addColumn('photo', function($row){
                                $photo = '<img src="' . url('/') . '/' . $row->photo .  '" width="24" class="rounded-circle">';
                                return "<a target='_blank' href='". url('/'). '/' . $row->photo ."' >$photo</a>";
                            })

            ->rawColumns(['action', 'checkbox', 'photo'])
            ->make(true);
        }

        $data['categories'] = DB::table('categories')->where('active',1)->get();

        return view('backend.products.index', $data);
    }
}
