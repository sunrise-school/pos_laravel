<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class InvoiceController extends Controller
{
    public function index(){
        $data['invoices'] = DB::table('invoices')
                        ->join('users','users.id','invoices.sale_by')
                        ->select('invoices.*','users.name as saleBy')
                        ->where('invoices.active',1)
                        ->get();
        
        return view('backend.invoices.index',$data);
    }
    public function detail($id){
        $data['invoice'] = DB::table("invoices")
                                ->join('users','users.id','invoices.sale_by')
                                ->where("invoices.id",$id)
                                ->select("invoices.*","users.name as saleBy")
                                ->first();
        $data['invoice_details'] = DB::table('invoice_details')->where('invoice_id',$id)->get();
        $data['company'] = DB::table("companies")->find(1);

        return view('backend.invoices.detail',$data);
    }
}
