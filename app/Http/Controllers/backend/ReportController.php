<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ReportController extends Controller
{
    public function invoice(Request $r){
        $fromDate = $r->fromDate ? date("Y-m-d H:i:s", strtotime($r->fromDate)) : date('Y-m-d 00:00:00');
        $toDate = $r->toDate ? date("Y-m-d H:i:s", strtotime($r->toDate)) : date("Y-m-d 23:59:59");

        $queryTotalOriginalPrice = DB::table("invoice_details")
                                        ->groupBy("invoice_id")
                                        ->select(
                                            DB::raw("SUM(product_original_price * qty) as total_original_price"),
                                            "invoice_id"
                                        )->toSql();
        $data['invoices'] = DB::table("invoices")
                    ->join(DB::raw("($queryTotalOriginalPrice) as subQuery"), "subQuery.invoice_id", "invoices.id")
                    ->whereBetween("invoices.created_at",[$fromDate, $toDate])
                    ->select("invoices.*", "subQuery.total_original_price as total_original_price")
                    ->get();
        $data['fromDate'] = $fromDate;
        $data['toDate'] = $toDate;


        return view("backend.reports.invoice", $data);
    }
    public function sale(Request $r){
        $fromDate = $r->fromDate ? date("Y-m-d H:i:s", strtotime($r->fromDate)) : date('Y-m-d 00:00:00');
        $toDate = $r->toDate ? date("Y-m-d H:i:s", strtotime($r->toDate)) : date("Y-m-d 23:59:59");

        $data['invoice_details'] = DB::table('invoice_details')->get();
        $data['invoices'] = DB::table('invoices')
                        ->whereBetween("created_at", [$fromDate, $toDate])
                        ->get();


        $data['fromDate'] = $fromDate;
        $data['toDate'] = $toDate;
        return view("backend.reports.sale", $data);
    }
}
