<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class DataTableController extends Controller
{
    
    public function insert(Request $r){
        if($r->ajax()){
            $data = $r->except('_token','tbl');

            if($r->photo){
                $data['photo'] = $r->photo->store('/image','custom');
            }
            $data['created_by'] = session()->get('user')->id;

            $insert = DB::table($r->tbl)->insert($data);
            if($insert){
                return response()->json(['status' => 'success', 'message' => "insert successfully"],200);
            } else {
                return response()->json(['status' => 'error', 'message' => "insert fail"],200);
            }
        }
    }

    public function edit(Request $r, $id,$tbl){
        if($r->ajax()){
            $data = DB::table($tbl)->find($id);
            return response()->json(['status' => 'success', 'data' => $data]);
        }
    }
    public function update(Request $r){
        if($r->ajax()){
            $data = $r->except('_token','tbl','eid','photo');

            if($r->photo){
                $data['photo'] = $r->photo->store('/image','custom');
            }
            $data['updated_by'] = session()->get('user')->id;

            $update = DB::table($r->tbl)
                        ->where('id',$r->eid)
                        ->update($data);

            if($update){
                return response()->json(['status' => 'success', 'message' => "update successfully"],200);
            } else {
                return response()->json(['status' => 'error', 'message' => "update fail"],200);
            }
        }
    }
    public function delete(Request $r){
        if($r->ajax()){
            $updateBy = session()->get('user')->id;

            $d = DB::table($r->tbl)
                    ->where("id",$r->eid)
                    ->update(['active' => 0, 'updated_by' => $updateBy]);
            if($d){
                return response()->json(['status' => 'success', 'message' => "Delete successfully"],200);
            } else {
                return response()->json(['status' => 'error', 'message' => "Delete fail"],200);
            }
        }
    }

    public function deleteAll(Request $r){
        if($r->ajax()){
            $d = DB::table($r->tbl)
                    ->whereIn("id",$r->ids)
                    ->update(['active' => 0, 'updated_by' => session()->get('user')->id]);
            if($d){
                return response()->json(['status' => 'success', 'message' => "Delete successfully"],200);
            } else {
                return response()->json(['status' => 'error', 'message' => "Delete fail"],200);
            }
        }
    }

}
