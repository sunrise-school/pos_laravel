<?php

function user($con){
    $u = DB::table('users')->find(session()->get('user')->id);
    
    $v = '';
    switch ($con) {
        case "id":
            $v = $u->id;
            break;
        case "username":
            $v = $u->username;
            break;
        case "photo":
            $v = $u->photo;
            break;
        case "name":
            $v = $u->name;
            break;
        default:
            $v = '';
    };

    return $v;
}

function company($con){
    $com = DB::table("companies")->find(1);
    $v = '';
    switch ($con) {
        case "name":
            $v = $com->name;
            break;
        case "photo":
            $v = $com->photo;
            break;
        default:
            $v = '';
    };

    return $v;
}

function checkPermission($permission, $check){
    $role_id = session()->get('user')->role_id;
    $data = DB::table("role_permissions")
                ->join('permissions','permissions.id','role_permissions.permission_id')
                ->where('permissions.name',$permission)
                ->where('role_permissions.active',1)
                ->where('role_permissions.role_id',$role_id)
                ->first();
    if($check == "list"){
        return $data->list;
    } else if($check == 'insert'){
        return $data->insert;
    } else if($check == 'update'){
        return $data->update;
    } else {
        return $data->delete;
    }
}