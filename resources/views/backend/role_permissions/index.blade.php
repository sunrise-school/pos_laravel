@extends('backend.layouts.master')
@section('title')
    role permission page
@endsection
@section('content-title')
    <h2> Role Permission</h2>
@endsection
@section('content')
    <div class="row">
        <div class="col-12" id="showAlert"></div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('generatePermission',$role_id) }}" class="btn btn-sm btn-warning mb-4">Regenerate permission</a>
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-sm table-bordered table-hover" id="dataTable" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Permission Name</th>
                                        <th>List</th>
                                        <th>Insert</th>
                                        <th>Update</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                {{-- <tbody id="userData"></tbody> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.roles.edit')
    @include('backend.roles.create')
@endsection
@section('js')
    <script>
        var table = $('#dataTable').DataTable({
            responsive: true,
            pageLength: 10,
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: `{{ route('admin.role_permission',$role_id) }}`,
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'permission_name',
                    name: 'permissions.name'
                },
                {
                    data : 'list',
                    render: function (data, type, row) {
                        return `<input type='checkbox' ${row.list == 1 ? 'checked' : ''} onclick="checkRolePermission(${row.id},'list',${row.list})" />`
                    },
                    orderable: false,
                    searchable: false
                },
                {
                    data : 'insert',
                    render: function (data, type, row) {
                        return `<input type='checkbox' ${row.insert == 1 ? 'checked' : ''} onclick="checkRolePermission(${row.id},'insert',${row.insert})" />`
                    },
                    orderable: false,
                    searchable: false
                },
                {
                    data : 'update',
                    render: function (data, type, row) {
                        return `<input type='checkbox' ${row.update == 1 ? 'checked' : ''} onclick="checkRolePermission(${row.id},'update',${row.update})" />`
                    },
                    orderable: false,
                    searchable: false
                },
                {
                    data : 'delete',
                    render: function (data, type, row) {
                        return `<input type='checkbox' ${row.delete == 1 ? 'checked' : ''} onclick="checkRolePermission(${row.id},'delete',${row.delete})" />`
                    },
                    orderable: false,
                    searchable: false
                },
            ]

        });

        function edit(obj) {
            const id = $(obj).attr('btnedit');
            const tbl = $(obj).attr('tbl');
            const url = burl + "/admin/bulk/getDetail/" + id + "/" + tbl;

            $.ajax({
                type: "GET",
                url: url,
                dataType: 'json',
                success: function(res) {
                    if (res.status == "success") {
                        $('#ename').val(res.data.name);
                        $('#eid').val(res.data.id);
                        $('#edit').modal();
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        }

        function createRole(e) {
            e.preventDefault();
            const data = $('#createRole').serialize();

            $.ajax({
                type: "POST",
                url: burl + "/admin/role-insert",
                data: data,
                dataType: 'json',
                success: function(res) {
                    if (res.status == "success") {
                        $('#create').modal('hide');
                        $("#showAlert").html(
                            `
                                        <div class="alert alert-success">${res.message}</div>
                                    `
                        );
                        $('#showAlert').show(500);
                        setTimeout(() => {
                            $('#showAlert').hide(500);
                        }, 1000);


                        table.ajax.reload();

                    } else {
                        alert(res.message);
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            })
        }

        function deleteRole(e) {
            const eid = $(e).attr('btndelete');
            let con = confirm("Are you sure ?");
            if (con) {
                $.ajax({
                    type: "GET",
                    url: burl + `/admin/role-delete?eid=${eid}`,
                    dataType: 'json',
                    success: function(res) {
                        if (res.status == "success") {
                            $("#showAlert").html(
                                `
                                    <div class="alert alert-success">${res.message}</div>
                                `
                            );

                            $('#showAlert').show(500);
                            setTimeout(() => {
                                $('#showAlert').hide(500);
                            }, 1000);
                            table.ajax.reload();
                        } else {
                            alert(res.message);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            }
        }

        function checkRolePermission(id, col, value){
            $.ajax({
                type: "GET",
                url: burl + `/admin/update-role-permission?id=${id}&col=${col}&value=${value}`,
                dataType: 'json',
                success: function(res) {
                    if (res.status == "success") {
                        $("#showAlert").html(
                            `
                                <div class="alert alert-success">${res.message}</div>
                            `
                        );

                        $('#showAlert').show(500);
                        setTimeout(() => {
                            $('#showAlert').hide(500);
                        }, 1000);
                        table.ajax.reload();
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        }
    </script>
@endsection
