<form method="POST" action="{{ route('admin.profile.update') }}" enctype="multipart/form-data">
    @csrf
    <div class="modal fade" id="edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    
                <div class="modal-body">
                    <div class="mb-1">
                        <label for="name" class="form-group">Name</label>
                        <input type="text" value="{{ $user->name }}" name="name" id="name" class="form-control">
                    </div>
                    <div class="mb-1">
                        <label for="address" class="form-group">Username</label>
                        <input type="text" value="{{ $user->username }}" name="username" id="address" class="form-control">
                    </div>
                    <div class="mb-1">
                        <label for="phone" class="form-group">Password</label>
                        <input type="password" value="{{ $user->password }}" name="password" id="phone" class="form-control">
                    </div>
                    <div class="mb-1">
                        <label for="photo" class="form-group">Photo</label>
                        <input type="file" name="photo" id="photo" class="form-control">
                    </div>
                    <div class="mb-1">
                        <label for="img" class="form-group">Your Image</label>
                        <div class="w-100 text-center">
                            <img src="{{ asset($user->photo) }}" alt="" class="rounded-circle" width="150" height="150">
                        </div>
                    </div>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    
</form>