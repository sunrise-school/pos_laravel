@extends('backend.layouts.master')
@section('title')
    invoice page
@endsection
@section('content-title')
    <h2> Invoice </h2>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h2>List</h2>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Code</th>
                                <th>Total Price</th>
                                <th>Sale by</th>
                                <th>Note</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($invoices as $index => $item)
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ sprintf("%04d",$item->code) }}</td>
                                    <td>{{ $item->total_price }}</td>
                                    <td>{{ $item->saleBy }}</td>
                                    <td>{{ $item->note }}</td>
                                    <td>
                                        <a href="{{ route("product.sale.print", $item->id) }}" target="_blank" class="btn btn-sm btn-outline-success">
                                            <i class="fa fa-print"></i>
                                        </a>
                                        &nbsp;
                                        <a href="{{ route("product.invoice.detail", $item->id) }}" class="btn btn-outline-primary btn-sm">
                                            <i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      
@endsection
@section('js')

@endsection