@extends('backend.layouts.master')
@section('title')
    invoice detail page
@endsection
@section('content-title')
    <h2> Invoice Detail </h2>
@endsection
@section('style')
    <style>
        @media print {
            .btn-print {
                display: none;
            }
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header btn-print">
                    <a href="{{ route("product.invoice") }}" class="btn-print btn-danger btn-sm btn">Back</a>
                    &nbsp;
                    <button class="btn btn-sm btn-primary btn-print" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-8">
                            <div class="row w-100 h-100">
                                <div class="col-4 w-100 h-100 border-right">
                                    <div class="w-100 p-0 h-100 d-flex align-items-center justify-content-center">
                                        <img src="{{ asset($company->photo) }}" class="rounded-circle" width="200"> 
                                    </div>
                                </div>
                                <div class="border-right col-8 d-flex align-items-center justify-content-center flex-column">
                                    <div class="h-100">
                                        <h2 class="text-bold">{{ $company->name }}</h2>
                                        <p>Address : {{ $company->address }}</p>
                                        <p>Phone : {{ $company->phone }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <h2>Invoice Detail</h2>
                            <table class="table" width="100%">
                                <tr>
                                    <td>Invoice Code</td>
                                    <td>:</td>
                                    <td>{{ sprintf("%04d", $invoice->code) }}</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>:</td>
                                    <td>Paid</td>
                                </tr>
                                <tr>
                                    <td>Sale By</td>
                                    <td>:</td>
                                    <td>{{ $invoice->saleBy }}</td>
                                </tr>
                                <tr>
                                    <td>Total Price</td>
                                    <td>:</td>
                                    <td>$ {{ $invoice->total_price }}</td>
                                </tr>
                                <tr>
                                    <td>Payment Method</td>
                                    <td>:</td>
                                    <td>{{ $invoice->payment_method_name }}</td>
                                </tr>
                            </table>
                        </div>
                        <h2 class="my-4">Details</h2>
                        <div class="col-12 p-0">
                            <table class="table table-sm table-bordered table-hover"> 
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Item</th>
                                        <th>Qty</th>
                                        <th>Total Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $t = 0;   
                                    @endphp
                                    @foreach ($invoice_details as $index => $item)
                                        <tr>
                                            <td>{{ $index + 1 }}</td>
                                            <td>{{ $item->product_name }}</td>
                                            <td>{{ $item->qty }}</td>
                                            <td>$ {{ $item->total_price }}</td>
                                        </tr>
                                        @php
                                            $t += $item->total_price
                                        @endphp
                                    @endforeach
                                    <tr>
                                        <td colspan="3" class="text-right">Grand Total :</td>
                                        <td>$ {{ $t }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')

@endsection