<!-- Modal -->
<form action="" id="updateDate" onsubmit="updateDate(event)">
    @csrf
    <input type="hidden" name="tbl" value="roles">
    <input type="hidden" name="eid" id="eid">
    <div class="modal fade" id="edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Role</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="eid">
                <div class="mb-1">
                    <label for="ename" class="form-group">Name</label>
                    <input type="text" name="name" id="ename" class="form-control">
                </div>
                <div class="mb-1">
                    <img id="eimg" src="" alt="" width="120">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="save" >Save changes</button>
            </div>
            </div>
        </div>
        </div>
</form>