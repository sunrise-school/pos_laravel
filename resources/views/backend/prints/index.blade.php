@component('backend.layouts.header')
    @slot('title')
        Print Invoice
    @endslot
    @slot('style')
       <style>
            .txt-upper {
                text-transform: uppercase;
            }
       </style>
    @endslot
@endcomponent
<body class="hold-transition sidebar-mini">

    <center>
        <img src="{{ asset($company->photo) }}" alt="" class="rounded-circle" width="150">
        <h2 class="text-bold">{{ $company->name }}</h2>
        <p class="m-0 p-0">Address : {{ $company->address }}</p>
        <small>
            <i>Phone : {{ $company->phone }}</i>
        </small>
    </center>
    <div class="row mt-4">
        <div class="col">
            <h4 class="text-bold txt-upper text-left">Invoice ID : {{ sprintf('%04d', $invoice->code) }}</h4>
        </div>
        <div class="col">
            <h4 class="text-bold txt-upper text-right">Sale Status : Paid</h4>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h4 class="text-bold txt-upper text-left">Payment Method : {{ $invoice->payment_method_name }}</h4>
        </div>
        <div class="col">
            <h4 class="text-bold txt-upper text-right">Sale By : {{ $sale_by }}</h4>
        </div>
        <div class="col-12">
            <p>
               Note : {{ $invoice->note }}
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Item</th>
                        <th>Price</th>
                        <th>Qty</th>
                        <th>Total Price</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $gTotal = 0;   
                    @endphp
                    @foreach ($invoice_details as $key => $item)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $item->product_name }}</td>
                            <td>$ {{ $item->product_price }}</td>
                            <td>{{ $item->qty }}</td>
                            <td>$ {{ $item->total_price }}</td>
                        </tr>
                        @php
                            $gTotal += $item->total_price
                        @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" class="text-right">Grand Total :</td>
                        <td>$ {{ $gTotal }}</td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="col-12 text-center">
            <p class="m-0">wifi : {{ $company->name }}</p>
            <b>12345678</b>
            <br>
            <br>
            <i>
                សូមអរគុណ
            </i>
        </div>
    </div>
    
    
    <script>
        window.print();
    </script>
</body>
@include('backend.layouts.footer')
