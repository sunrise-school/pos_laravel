@extends('backend.layouts.master')
@section('title')
    user page
@endsection
@section('content-title')
    <h2> Users </h2>
@endsection
@section('content')
    <div class="row">
        <div class="col-12" id="showAlert"></div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-12">
                            <button class="btn btn-sm btn-primary" id="btnCreate" data-toggle="modal" data-target="#create"> <i class="fa fa-plus"></i> Create</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-sm table-bordered table-hover" id="dataTable" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Username</th>
                                        <th>Role</th>
                                        <th>photo</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                {{-- <tbody id="userData"></tbody> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.users.edit')
    @include('backend.users.create')
@endsection
@section('js')
    <script>
        function fetchData() {
            $.ajax({
                type: "GET",
                url: burl + "/admin/user",
                dataType: 'json',
                success: function(res) {
                    let data = '';
                    let i = 1;
                    res.user.forEach(item => {
                        data += `
                        <tr>
                            <td>${i}</td>  
                            <td>${item.name}</td>  
                            <td>${item.username}</td> 
                            <td>${item.role_name}</td>  
                            <td>
                                <img src="${burl + "/" + item.photo}" width="24" class="rounded-circle">
                            </td>  
                            <td>
                                <button type="button" btnedit="${item.id}" class="btn btn-sm btn-success" onclick="edit(this)"> <i class="fa fa-edit"></i> </button>
                                <button type="button" btndelete="${item.id}" class="btn btn-sm btn-danger" onclick="deleteUser(this)" > <i class="fa fa-trash"></i> </button>
                            </td>
                        </tr>  
                    `;
                        i++;
                    });
                    $("#userData").html(data);
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        }


        function edit(obj) {
            const id = $(obj).attr('btnedit');
            $.ajax({
                type: "GET",
                url: burl + "/admin/user/edit/" + id,
                dataType: 'json',
                success: function(res) {
                    $('#username').val(res.userDetail.username);
                    $('#password').val(res.userDetail.password);
                    $('#eid').val(res.userDetail.id);
                    $(`#erole option[value=${res.userDetail.role_id}]`).attr("selected", true);
                    $('#edit').modal();  
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        }

        function deleteUser(obj) {
            const id = $(obj).attr('btndelete');
            let con = confirm("Are you sure ?");
            if(con){
                $.ajax({
                type: "GET",
                url: burl + "/admin/user/delete?id=" + id,
                dataType: 'json',
                success: function(res) {
                    if(res.status == "success"){
                        $('#edit').modal('hide');
                        $("#showAlert").html(
                            `
                                <div class="alert alert-success">${res.message}</div>
                            `
                        );
                        // fetchData();
                        table.ajax.reload();
                    } else {
                        alert(res.message);
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
            }
        }

        // fetchData();

        var btnSubmit = document.getElementById('save');
        btnSubmit.addEventListener('click', function() {
            const username = $("#username").val();
            const password = $("#password").val();
            const role_id = $("#erole").val();

            const id = $("#eid").val();
            
            $.ajax({
                type: "POST",
                headers: {
                            'X-CSRF-TOKEN': "{{csrf_token()}}",
                        },
                url: burl + "/admin/user/update/" + id,
                data : {
                    username : username,
                    password : password,
                    role_id : role_id
                },
                dataType: 'json',
                success: function(res) {
                    console.log(res);
                    if(res.status == "success"){
                        $('#edit').modal('hide');
                        $('#showAlert').html(res.message);
                        table.ajax.reload();

                        // fetchData();
                    } else {
                        alert(res.message);
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });

        function createUser(e){
            e.preventDefault();
            const data = $('#formSubmit').serialize();

            $.ajax({
                type: "GET",
                url: burl + "/admin/user/create/?" + data,
                dataType: 'json',
                success: function(res) {
                    if(res.status == "success"){
                        $('#create').modal('hide');

                        $("#showAlert").html(
                            `
                                <div class="alert alert-success">${res.message}</div>
                            `
                        );

                        table.ajax.reload();


                        // fetchData();
                    } else {
                        alert(res.message);
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        }

        var table = $('#dataTable').DataTable({
            responsive: true, 
            pageLength: 2,
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: "{{ route('admin.user') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'username', name: 'username'},
                {data: 'role_name', name: 'roles.name'},
                {
                    data: 'photo',
                    name: 'photo',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        
        });
    </script>
@endsection
