<!-- Modal -->
<div class="modal fade" id="edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="eid">
            <div class="mb-1">
                <label for="username" class="form-group">Username</label>
                <input type="text" name="username" id="username" class="form-control">
            </div>
            <div class="mb-1">
                <label for="password" class="form-group">Password</label>
                <input type="password"  name="password" id="password" class="form-control">
            </div>
            <div class="mb-1">
                <label for="erole" class="form-group">Role</label>
                <select name="role_id" id="erole" class="form-control" required>
                    @foreach ($roles as $r)
                        <option value="{{ $r->id }}">{{ $r->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="save" >Save changes</button>
        </div>
        </div>
    </div>
    </div>