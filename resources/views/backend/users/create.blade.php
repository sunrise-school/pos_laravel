<form method="POST" id="formSubmit" enctype="multipart/form-data" onsubmit="createUser(event)">
    <div class="modal fade" id="create" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    
                <div class="modal-body">
                    <div class="mb-1">
                        <label for="name" class="form-group">Name</label>
                        <input type="text" name="name" id="name" class="form-control">
                    </div>
                    <div class="mb-1">
                        <label for="address" class="form-group">Username</label>
                        <input type="text" name="username" id="address" class="form-control">
                    </div>
                    <div class="mb-1">
                        <label for="role" class="form-group">Role</label>
                        <select name="role_id" id="role" class="form-control" required>
                            <option value="">Please Select One</option>
                            @foreach ($roles as $r)
                                <option value="{{ $r->id }}">{{ $r->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-1">
                        <label for="phone" class="form-group">Password</label>
                        <input type="password" name="password" id="phone" class="form-control">
                    </div>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    
</form>