@extends('backend.layouts.master')
@section('title')
    permission page
@endsection
@section('content-title')
    <h2> Permissions </h2>
@endsection
@section('content')
    <div class="row">
        <div class="col-12" id="showAlert"></div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-12">
                            <button class="btn btn-sm btn-primary" id="btnCreate" data-toggle="modal" data-target="#create">
                                <i class="fa fa-plus"></i> Create</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-sm table-bordered table-hover" id="dataTable" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Permission</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                {{-- <tbody id="userData"></tbody> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.permissions.edit')
    @include('backend.permissions.create')
@endsection
@section('js')
    <script>
        var table = $('#dataTable').DataTable({
            responsive: true,
            pageLength: 10,
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: "{{ route('admin.permission') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'alias',
                    name: 'alias'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]

        });

        function edit(obj) {
            const id = $(obj).attr('btnedit');
            const tbl = $(obj).attr('tbl');
            const url = burl + "/admin/bulk/getDetail/" + id + "/" + tbl;

            $.ajax({
                type: "GET",
                url: url,
                dataType: 'json',
                success: function(res) {
                    if (res.status == "success") {
                        $('#ename').val(res.data.name);
                        $('#ealias').val(res.data.alias);
                        $('#eid').val(res.data.id);
                        $('#edit').modal();
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        }

        function createPermission(e) {
            e.preventDefault();
            const data = $('#createPermission').serialize();

            $.ajax({
                type: "POST",
                url: burl + "/admin/permission-insert",
                data: data,
                dataType: 'json',
                success: function(res) {
                    if (res.status == "success") {
                        $('#create').modal('hide');
                        $("#showAlert").html(
                            `
                                        <div class="alert alert-success">${res.message}</div>
                                    `
                        );
                        $('#showAlert').show(500);
                        setTimeout(() => {
                            $('#showAlert').hide(500);
                        }, 1000);


                        table.ajax.reload();

                    } else {
                        alert(res.message);
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            })
        }

        function deletePermission(e) {
            const eid = $(e).attr('btndelete');
            let con = confirm("Are you sure ?");
            if (con) {
                $.ajax({
                    type: "GET",
                    url: burl + `/admin/permission-delete?eid=${eid}`,
                    dataType: 'json',
                    success: function(res) {
                        if (res.status == "success") {
                            $("#showAlert").html(
                                `
                                    <div class="alert alert-success">${res.message}</div>
                                `
                            );

                            $('#showAlert').show(500);
                            setTimeout(() => {
                                $('#showAlert').hide(500);
                            }, 1000);
                            table.ajax.reload();
                        } else {
                            alert(res.message);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            }
        }
    </script>
@endsection
