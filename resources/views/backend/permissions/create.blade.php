<form method="POST" id="createPermission" onsubmit="createPermission(event)">
    @csrf
    <div class="modal fade" id="create" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Permission</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    
                <div class="modal-body">
                    <div class="mb-1">
                        <label for="alias" class="form-group">Name</label>
                        <input type="text" name="alias" id="alias" class="form-control" required>
                    </div>
                    <div class="mb-1">
                        <label for="name" class="form-group">Permission</label>
                        <input type="text" name="name" id="name" class="form-control" required>
                    </div>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    
</form>