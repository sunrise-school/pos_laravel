@component('backend.layouts.header')
    @slot('title')
        Login
    @endslot
    @slot('style')
        <style>
            .page{
                width: 100vw;
                height: 100vh;
                position: absolute;
                background-image: url("https://t4.ftcdn.net/jpg/03/99/30/73/360_F_399307378_LuSoCJrfkRn2jAGaByqAi771EF1QTwdf.jpg");
                object-fit: cover;
                background-position: center;
                background-size: cover;
                background-repeat: no-repeat;
            }
        </style>
    @endslot
@endcomponent
<body class="hold-transition login-page page">
    <div class="login-box">
      <div class="card z-10">
        <div class="card-body login-card-body rounded">
          <h2 class="login-box-msg">Login</h2>
          @if(session()->has('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
        @endif
          <form action="{{ route('auth.login') }}" method="post">
            @csrf
            <div class="input-group mb-3">
              <input type="text" name="username" class="form-control" placeholder="Enter your username" value="{{ old('username') }}" required>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-envelope"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" name="password" class="form-control" placeholder="Password" value="{{ old('password') }}"  required>
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block">Sign In</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>
@include('backend.layouts.footer')