@extends('backend.layouts.master')
@section('title')
    Sale page
@endsection
@section('content-title')
    <h2> Sale Report </h2>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <form action="{{ route('sale-report.fillter') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-4">
                                <label for="fromDate" class="form-group mb-1">From Date</label>
                                <input type="datetime-local" id="fromDate" value="{{ $fromDate }}" name="fromDate"
                                    class="form-control">
                            </div>
                            <div class="col-4">
                                <label for="toDate" class="form-group mb-1">To Date</label>
                                <input type="datetime-local" id="toDate" value="{{ $toDate }}" name="toDate"
                                    class="form-control">
                            </div>
                            <div class="col">
                                <label for="" class="form-group mb-1">&nbsp;</label>
                                <button class="btn btn-sm btn-primary form-control">Search</button>
                            </div>
                            <div class="col"></div>
                            <div class="col"></div>
                            <div class="col"></div>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Code</th>
                                <th>Item</th>
                                <th>Qty</th>
                                <th>Cost</th>
                                <th>Total Cost</th>
                                <th>Total Price</th>
                                <th>Profit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $tOriginal = 0;
                                $tQty = 0;
                                $tCost = 0;
                                $tPrice = 0;
                                $count = 0;
                            @endphp
                            @foreach ($invoice_details as $key => $inv_detail)
                                @foreach ($invoices as $index => $inv)
                                    @if ($inv->id == $inv_detail->invoice_id)
                                        <tr>
                                            <td>{{ $count + 1 }}</td>
                                            <td>{{ sprintf('%04d', $inv->code) }}</td>
                                            <td>{{ $inv_detail->product_name }}</td>
                                            <td>{{ $inv_detail->qty }}</td>
                                            <td>$ {{ $inv_detail->product_original_price }}</td>
                                            <td>$ {{ $inv_detail->qty * $inv_detail->product_original_price }}</td>
                                            <td>$ {{ $inv_detail->total_price }}</td>
                                            <td>$
                                                {{ $inv_detail->total_price - $inv_detail->qty * $inv_detail->product_original_price }}
                                            </td>
                                        </tr>
                                        @php
                                            $tOriginal += $inv_detail->product_original_price;
                                            $tQty += $inv_detail->qty;
                                            $tCost += $inv_detail->qty * $inv_detail->product_original_price;
                                            $tPrice += $inv_detail->total_price;
                                            $count += 1;
                                        @endphp
                                    @endif
                                @endforeach
                            @endforeach
                            <tr>
                                <td colspan="3" class="text-right text-bold">Total :</td>
                                <td>{{ $tQty }}</td>
                                <td>$ {{ $tOriginal }}</td>
                                <td>$ {{ $tCost }}</td>
                                <td>$ {{ $tPrice }}</td>
                                <td>$ {{ $tPrice - $tCost }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection
