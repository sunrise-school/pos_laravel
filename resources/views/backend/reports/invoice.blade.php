@extends('backend.layouts.master')
@section('title')
    report page
@endsection
@section('content-title')
    <h2> Invoice Report </h2>
@endsection
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <form action="{{ route("invoice-report.fillter") }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-4">
                                <label for="fromDate" class="form-group mb-1">From Date</label>
                                <input type="datetime-local" id="fromDate" value="{{ $fromDate }}" name="fromDate" class="form-control">
                            </div>
                            <div class="col-4">
                                <label for="toDate" class="form-group mb-1">To Date</label>
                                <input type="datetime-local" id="toDate" value="{{ $toDate }}" name="toDate" class="form-control">
                            </div>
                            <div class="col">
                                <label for="" class="form-group mb-1">&nbsp;</label>
                                <button class="btn btn-sm btn-primary form-control">Search</button>
                            </div>
                            <div class="col"></div>
                            <div class="col"></div>
                            <div class="col"></div>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Code</th>
                                <th>Payment Name</th>
                                <th>Total Original Price</th>
                                <th>Total Sale Price</th>
                                <th>Total Profit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $tOriginal = 0;
                                $tPrice = 0;
                            @endphp
                            @foreach ($invoices as $index => $item)
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ sprintf("%04d",$item->code) }}</td>
                                    <td>{{ $item->payment_method_name }}</td>
                                    <td>$ {{ $item->total_original_price }}</td>
                                    <td>$ {{ $item->total_price }}</td>
                                    <td>$ {{ $item->total_price - $item->total_original_price }}</td>
                                </tr>
                                @php
                                    $tOriginal += $item->total_original_price;
                                    $tPrice += $item->total_price;
                                @endphp
                            @endforeach
                            <tr>
                                <td colspan="3" class="text-right text-md text-bold">Total :</td>
                                <td>$ {{ $tOriginal }}</td>
                                <td>$ {{ $tPrice }}</td>
                                <td>$ {{ $tPrice - $tOriginal }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
      
@endsection
@section('js')

@endsection