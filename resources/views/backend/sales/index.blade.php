@extends('backend.layouts.master')
@section('title')
    Sale Page
@endsection
@section('content-title')
    <h2> Sale Product </h2>
@endsection
@section('content')
    <div class="row mb-3">
        <div class="col-12" id="showAlert"></div>
        <div class="col-4">
            <label for="product">Product</label>
            <select id="product" class="form-control">
                @foreach ($products as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-4">
            <label for="qty">Qty</label>
            <input id="qty" type="number" min="0" class="form-control" placeholder="input qty">
        </div>
        <div class="col-4">
            <label for="method">Payment</label>
            <select id="method" class="form-control">
                @foreach ($methods as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col mt-4">
            <button onclick="addProduct()" class="btn btn-sm btn-success">
                <i class="fa fa-plus mr-2"></i>
                Add
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered bg-white table-hover table-sm">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Product Name</th>
                        <th>qty</th>
                        <th>Price</th>
                        <th>Total</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="productData"></tbody>
            </table>
        </div>
        <div class="col-12 text-right">
            <button type="button" onclick="submitDataSaleProduct()" class="btn btn-outline-dark">Submit</button>
        </div>
    </div>
    @include('backend.roles.edit')
    @include('backend.roles.create')
@endsection
@section('js')
    <script>
        const products = {!! json_encode($products) !!};
        var data = [];
        var amount = 0;

        function addProduct() {

            const producId = $("#product").val();
            const qty = $("#qty").val();

            if (qty == 0 || qty == '') return;
            const productDetail = products.find((item) => item.id == producId);

            const checkIndex = data.findIndex((item) => item.id == producId);


            if (checkIndex != -1) {
                // update
                data[checkIndex].qty = Number(data[checkIndex].qty) + Number(qty);
            } else {
                let items = {
                    id: productDetail.id,
                    name: productDetail.name,
                    original_price: productDetail.original_price,
                    photo: productDetail.photo,
                    price: productDetail.price,
                    qty: qty
                }
                data.push(items);
            }

            showOnTable();
        }

        function showOnTable() {
            // console.log(data);
            let tb = '';
            amount = 0;
            
            data.forEach((item, index) => {
                tb += `
                    <tr>
                        <td> ${index + 1} </td>
                        <td> ${item.name} </td>
                        <td> ${item.qty} </td>
                        <td> $ ${item.price} </td>
                        <td> $ ${Number(item.qty) * Number(item.price)} </td>
                        <td> 
                            <button onclick="removeData(${item.id})" class="btn btn-sm btn-outline-danger">
                                <i class="fa fa-trash"></i>    
                            </button>
                        </td>
                    </tr>
                `;
                amount = Number(amount) + (Number(item.qty) * Number(item.price));
            });

            tb += `
                <tr>
                    <td colspan="4" class="text-right">
                        Grand Total :
                    </td>
                    <td>
                        $ ${amount}
                    </td>
                    <td></td>
                </tr>
            `

            $('#productData').html(tb);
        }

        function removeData(id) {
            data = data.filter((item) => item.id != id);
            showOnTable();
        }

        function submitDataSaleProduct() {
            if (data.length == 0) {
                alert('no data');
            } else {
                const methods = {!! json_encode($methods) !!};
                const saleProducts = {
                    items : data,
                    amount : amount,
                    payment : methods.find((item) => item.id == $('#method').val()),
                    _token : "{{ csrf_token() }}"
                }
                $.ajax({
                    type: "POST",
                    url: burl + "/admin/product/sale/insert",
                    data: saleProducts,
                    dataType: 'json',
                    success: function(res) {
                        if (res.status == "success") {
                            data = [];
                            amount = 0;
                            showOnTable();
                            $("#showAlert").html(
                                `
                                    <div class="alert alert-success">${res.message}</div>
                                `
                            );

                            $('#showAlert').show(500);
                            setTimeout(() => {
                                $('#showAlert').hide(500);
                            }, 1000);

                            const redirectTo = burl + "/admin/print/"+res.id;
                            window.open(redirectTo,'_blank');

                        } else {
                            alert(res.message);
                        }
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            }
        }
    </script>
@endsection
