@extends('backend.layouts.master')
@section('title')
    banner page
@endsection
@section('content-title')
    <h2> Banner </h2>
@endsection
@section('content')
    <div class="row">
        <div class="col-12" id="showAlert"></div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-12">
                            <button class="btn btn-sm btn-primary" id="btnCreate" data-toggle="modal" data-target="#create"> <i class="fa fa-plus"></i> Create</button>
                            <button class="btn btn-sm btn-danger" id="btnDeleteAll" dtbl="banners" onclick="deleteAll(this)"> <i class="fa fa-plus"></i> Delete All</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-sm table-bordered table-hover" id="dataTable" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" id="checky" onclick="checkBox()"></th>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>photo</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                {{-- <tbody id="userData"></tbody> --}}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.banners.edit')
    @include('backend.banners.create')
@endsection
@section('js')
    <script>

        var table = $('#dataTable').DataTable({
            responsive: true, 
            pageLength: 10,
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: "{{ route('admin.banner') }}",
            columns: [
                {
                    data: 'checkbox',
                    name: 'checkbox',
                    orderable: false,
                    searchable: false
                },
                {data: 'DT_RowIndex',  orderable: false, searchable: false},
                {data: 'name', name: 'name'},
                {
                    data: 'photo',
                    name: 'photo',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        
        });

        function edit(obj){
            const id = $(obj).attr('btnedit');
            const tbl = $(obj).attr('tbl');
            const url = burl + "/admin/bulk/getDetail/" + id + "/" + tbl;

            $.ajax({
                type: "GET",
                url: url,
                dataType: 'json',
                success: function(res) {
                    if(res.status == "success"){
                        $('#ename').val(res.data.name);
                        $('#eid').val(res.data.id);
                        $('#eimg').attr('src', burl + '/' + res.data.photo);
                        $('#edit').modal();
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        }
    </script>
@endsection
