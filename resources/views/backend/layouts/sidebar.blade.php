<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  @php
    $com = DB::table("companies")->find(1);
    $u = DB::table('users')->find(session()->get('user')->id);
  @endphp
    <!-- Brand Logo -->
    <a href="{{ route('dashboard') }}" class="brand-link">
      <img src="{{ asset(company("photo")) }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">{{ company("name") }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          @if(user('photo'))
            <img src="{{ asset(user('photo')) }}" class="img-circle elevation-2" alt="User Image">
          @else
            <img src="{{ asset('adminlte/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
          @endif
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ user('name') }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{ route("dashboard") }}" class="nav-link {{ request()->path() == 'admin' ? 'active' : '' }}">
              <i class="nav-icon fas fa-th"></i>
              <p>{{ __('lb.dashboard') }}</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route("product.sale") }}" class="nav-link {{ request()->path() == 'admin/product/sale' ? 'active' : '' }}">
              <i class="nav-icon fas fa-th"></i>
              <p>Sale</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route("product.invoice") }}" class="nav-link {{ request()->path() == 'admin/product/invoice' ? 'active' : '' }}">
              <i class="nav-icon fas fa-th"></i>
              <p>Invoice</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route("invoice-report") }}" class="nav-link {{ request()->path() == 'admin/invoice-report' ? 'active' : '' }}">
              <i class="nav-icon fas fa-th"></i>
              <p>Invoice Report</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route("sale-report") }}" class="nav-link {{ request()->path() == 'admin/sale-report' ? 'active' : '' }}">
              <i class="nav-icon fas fa-th"></i>
              <p>Sale Report</p>
            </a>
          </li>
          <li class="nav-item {{ 
            request()->path() == 'admin/category' ||
            request()->path() == 'admin/payment-method' ||
            request()->path() == 'admin/video' || 
            request()->path() == 'admin/banner' || 
            request()->path() == 'admin/product'

            ? 'menu-open' : '' }}">
              <a href="#" class="nav-link {{ 
              request()->path() == 'admin/category' || 
              request()->path() == 'admin/payment-method' ||
              request()->path() == 'admin/video' ||
              request()->path() == 'admin/banner' ||
              request()->path() == 'admin/product'


              ? 'active' : '' }}">
                <i class="nav-icon fas fa-user-friends"></i>
                <p>
                  System Management
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ route("admin.category") }}" class="nav-link {{ request()->path() == 'admin/category' ? 'active' : '' }}">
                    <i class="fas fa-igloo nav-icon"></i>
                    <p>Product Category</p>
                  </a>
                </li>
                @if(checkPermission('product', 'list') == 1)
                <li class="nav-item">
                  <a href="{{ route("admin.product") }}" class="nav-link {{ request()->path() == 'admin/product' ? 'active' : '' }}">
                    <i class="fas fa-igloo nav-icon"></i>
                    <p>Product</p>
                  </a>
                </li>
                @endif
                <li class="nav-item">
                  <a href="{{ route("admin.payment_method") }}" class="nav-link {{ request()->path() == 'admin/payment-method' ? 'active' : '' }}">
                    <i class="fas fa-igloo nav-icon"></i>
                    <p>Payment Method</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route("admin.video") }}" class="nav-link {{ request()->path() == 'admin/videos' ? 'active' : '' }}">
                    <i class="fas fa-igloo nav-icon"></i>
                    <p>Video</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route("admin.banner") }}" class="nav-link {{ request()->path() == 'admin/banner' ? 'active' : '' }}">
                    <i class="fas fa-igloo nav-icon"></i>
                    <p>Banner</p>
                  </a>
                </li>
              </ul>
            </li>
          <li class="nav-item {{ 
          request()->path() == 'admin/company' || 
          request()->path() == 'admin/my-profile' ||
          request()->path() == 'admin/user' || 
          request()->path() == 'admin/permission' ||
          request()->path() == 'admin/role' 

          ? 'menu-open' : '' }}">
            <a href="#" class="nav-link {{ 
            request()->path() == 'admin/company'  ||
            request()->path() == 'admin/my-profile' ||
            request()->path() == 'admin/user' || 
            request()->path() == 'admin/permission' ||
            request()->path() == 'admin/role' 

            ? 'active' : '' }}">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                {{ trans('lb.setting') }}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route("admin.permission") }}" class="nav-link {{ request()->path() == 'admin/permission' ? 'active' : '' }}">
                  <i class="fas fa-igloo nav-icon"></i>
                  <p>Permission</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route("admin.role") }}" class="nav-link {{ request()->path() == 'admin/role' ? 'active' : '' }}">
                  <i class="fas fa-igloo nav-icon"></i>
                  <p>Role</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route("admin.user") }}" class="nav-link {{ request()->path() == 'admin/user' ? 'active' : '' }}">
                  <i class="fas fa-igloo nav-icon"></i>
                  <p>User</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route("admin.company") }}" class="nav-link {{ request()->path() == 'admin/company' ? 'active' : '' }}">
                  <i class="fas fa-igloo nav-icon"></i>
                  <p>Company Info</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route("admin.profile") }}" class="nav-link {{ request()->path() == 'admin/my-profile' ? 'active' : '' }}">
                  <i class="fas fa-igloo nav-icon"></i>
                  <p>My Profile</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>