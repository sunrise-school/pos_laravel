@component('backend.layouts.header')
    @slot('title')
        @yield('title')
    @endslot
    @slot('style')
        @yield('style')
    @endslot
@endcomponent
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        @include('backend.layouts.navbar')
        @include('backend.layouts.sidebar')
        <div class="content-wrapper">
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                    <div class="col-sm-6">
                        @yield('content-title')
                    </div>
                    </div>
                </div>
            </section>
            <section class="content">
                @yield('content')
            </section>
        </div>
        @include('backend.layouts.bottomPage')
    </div>
    <script src="{{ asset("adminlte/plugins/jquery/jquery.min.js") }}"></script>

    <script src="{{ asset("adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js") }}"></script>

    <script src="{{ asset("adminlte/plugins/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js") }}"></script>
    <script src="{{ asset("adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js") }}"></script>
    <script src="{{ asset("adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js") }}"></script>
    <script src="{{ asset("adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js") }}"></script>
    <script src="{{ asset("adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js") }}"></script>
    <script src="{{ asset("adminlte/plugins/jszip/jszip.min.js") }}"></script>
    <script src="{{ asset("adminlte/plugins/pdfmake/pdfmake.min.js") }}"></script>
    <script src="{{ asset("adminlte/plugins/pdfmake/vfs_fonts.js") }}"></script>
    <script src="{{ asset("adminlte/plugins/datatables-buttons/js/buttons.html5.min.js") }}"></script>
    <script src="{{ asset("adminlte/plugins/datatables-buttons/js/buttons.print.min.js") }}"></script>
    <script src="{{ asset("adminlte/plugins/datatables-buttons/js/buttons.colVis.min.js") }}"></script>
    <script src="{{ asset("adminlte/dist/js/adminlte.min.js") }}"></script>
    <script src="{{ asset("assets/js/custom.js") }}"></script>
    
    <script>
        var burl = "{{ url('/') }}"
    </script>
    @yield('js')
</body>
@include('backend.layouts.footer')
