<!-- Modal -->
<form action="" id="updateDate" onsubmit="updateDate(event)">
    @csrf
    <input type="hidden" name="tbl" value="products">
    <input type="hidden" name="eid" id="eid">
    <div class="modal fade" id="edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Banners</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="eid">
                <div class="mb-1">
                    <label for="ename" class="form-group">Name</label>
                    <input type="text" name="name" id="ename" class="form-control" required>
                </div>
                <div class="mb-1">
                    <label for="ecat" class="form-group">Category</label>
                    <select name="category_id" id="ecat" class="form-control" required>
                        <option value="">Please Select one</option>
                        @foreach ($categories as $cat)
                            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-1">
                    <label for="eoriginalPrice" class="form-group">Original Price</label>
                    <input type="number" step="0.001" min="0" name="original_price" id="eoriginalPrice" class="form-control" required>
                </div>
                <div class="mb-1">
                    <label for="eprice" class="form-group">Price</label>
                    <input type="number" step="0.001" min="0" name="price" id="eprice" class="form-control" required>
                </div>
                <div class="mb-1">
                    <label for="eshortDescription" class="form-group">Short Description</label>
                    <input type="text" name="shot_description" id="eshortDescription" class="form-control">
                </div>
                <div class="mb-1">
                    <label for="elongDescription" class="form-group">Long Description</label>
                    <input type="text" name="long_description" id="elongDescription" class="form-control">
                </div>
                <div class="mb-1">
                    <label for="ephoto" class="form-group">Photo</label>
                    <input type="file" name="photo" id="ephoto" class="form-control">
                </div>
                <div class="mb-1">
                    <img id="eimg" src="" alt="" width="120">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="save" >Save changes</button>
            </div>
            </div>
        </div>
        </div>
</form>