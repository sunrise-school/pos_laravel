<form method="POST" id="createData" enctype="multipart/form-data" onsubmit="createData(event)">
    @csrf
    <input type="hidden" name="tbl" value="products">
    <div class="modal fade" id="create" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    
                <div class="modal-body">
                    <div class="mb-1">
                        <label for="name" class="form-group">Name</label>
                        <input type="text" name="name" id="name" class="form-control" required>
                    </div>
                    <div class="mb-1">
                        <label for="cat" class="form-group">Category</label>
                        <select name="category_id" id="cat" class="form-control" required>
                            <option value="">Please Select one</option>
                            @foreach ($categories as $cat)
                                <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-1">
                        <label for="originalPrice" class="form-group">Original Price</label>
                        <input type="number" step="0.001" min="0" name="original_price" id="originalPrice" class="form-control" required>
                    </div>
                    <div class="mb-1">
                        <label for="price" class="form-group">Price</label>
                        <input type="number" step="0.001" min="0" name="price" id="price" class="form-control" required>
                    </div>
                    <div class="mb-1">
                        <label for="shotDescription" class="form-group">Short Description</label>
                        <input type="text" name="shot_description" id="shotDescription" class="form-control">
                    </div>
                    <div class="mb-1">
                        <label for="longDescription" class="form-group">Long Description</label>
                        <input type="text" name="long_description" id="longDescription" class="form-control">
                    </div>
                    <div class="mb-1">
                        <label for="photo" class="form-group">Photo</label>
                        <input type="file" name="photo" id="photo" class="form-control">
                    </div>
                </div>
    
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    
</form>