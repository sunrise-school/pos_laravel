$('#showAlert').hide();
function createData(e){
    e.preventDefault();
    // const data = $('#createData').serialize();
    const data = new FormData($("#createData")[0]);

    $.ajax({
        type: "POST",
        url: burl + "/admin/bulk/insert",
        data : data,
        dataType: 'json',
        async: false, 
        cache: false, 
        contentType: false, 
        processData: false, 
        success: function(res) {
            if(res.status == "success"){
                $('#create').modal('hide');            
                $("#showAlert").html(
                    `
                        <div class="alert alert-success">${res.message}</div>
                    `
                );
                $('#showAlert').show(500);
                setTimeout(() => {
                    $('#showAlert').hide(500);
                }, 1000);


                table.ajax.reload();

            } else {
                alert(res.message);
            }
        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
}

function updateDate(e){
    e.preventDefault();
    // const data = $('#updateDate').serialize();

    const data = new FormData($("#updateDate")[0]);

    $.ajax({
        type: "POST",
        url: burl + "/admin/bulk/update",
        data : data,
        dataType: 'json',
        async: false, 
        cache: false, 
        contentType: false, 
        processData: false, 
        success: function(res) {
            if(res.status == "success"){
                $('#edit').modal('hide');

                $("#showAlert").html(
                    `
                        <div class="alert alert-success">${res.message}</div>
                    `
                );

                $('#showAlert').show(500);
                setTimeout(() => {
                    $('#showAlert').hide(500);
                }, 1000);
                table.ajax.reload();

            } else {
                alert(res.message);
            }
        },
        error: function(data) {
            console.log('Error:', data);
        }
    });
}

function deleteData(obj){
    const eid = $(obj).attr('btndelete');
    const tbl = $(obj).attr('tbl');
    let con = confirm("Are you sure ?");
    if(con){
        $.ajax({
            type: "GET",
            url: burl + `/admin/bulk/delete?eid=${eid}&tbl=${tbl}`,
            dataType: 'json',
            success: function(res) {
                if(res.status == "success"){    
                    $("#showAlert").html(
                        `
                            <div class="alert alert-success">${res.message}</div>
                        `
                    );

                    $('#showAlert').show(500);
                    setTimeout(() => {
                        $('#showAlert').hide(500);
                    }, 1000);
                    table.ajax.reload();
                } else {
                    alert(res.message);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    }
}



function checkBox(){
    // let ids = [];
    $("#dataTable tbody tr td input[type='checkbox']").each(function(e) {
        if($("#checky").is(":checked")){
            $(this).prop('checked',true);
            // var id = $(this).attr("eid");
            // ids.push(id);   
        } else {
            $(this).prop('checked',false);
            // ids = [];
        }
    });
}

function deleteAll(obj){
    let ids = [];
    let tbl = $(obj).attr('dtbl');

    $("#dataTable tbody tr td input[type='checkbox']").each(function(e) {
        if($(this).is(":checked")){
            ids.push($(this).attr("eid")); 
        }
    });


    let con = confirm("Are you sure ?");
    if(con){
        if(ids.length > 0){
        $.ajax({
            type: "GET",
            url: burl + "/admin/bulk/deleteAll",
            data : {
                ids : ids,
                tbl : tbl
            },
            dataType: 'json',
            success: function(res) {
                if(res.status == "success"){
                    $("#showAlert").html(
                        `
                            <div class="alert alert-success">${res.message}</div>
                        `
                    );
                    $('#checky').prop('checked',false);
                    
                    $('#showAlert').show(500);
                    setTimeout(() => {
                        $('#showAlert').hide(500);
                    }, 1000);
                    table.ajax.reload();
                } else {
                    alert(res.message);
                }
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });
    }
    }
}

