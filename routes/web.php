<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\backend\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function(){
    Route::get('/login', 'backend\AuthController@login')->name('auth.login');
    Route::post('/login', 'backend\AuthController@login')->name('auth.login');
    Route::get('/logout', 'backend\AuthController@logout')->name('auth.logout');

    Route::middleware(['checkAuth'])->group(function () {
        Route::get('/', 'backend\HomeController@index')->name('dashboard');

        // company info
        Route::get('/company', 'backend\CompanyController@index')->name('admin.company');
        Route::post('/company/edit', 'backend\CompanyController@edit')->name('admin.company.edit');

        //my profile
        Route::get('/my-profile', 'backend\ProfileController@index')->name('admin.profile');
        Route::post('/update-profile', 'backend\ProfileController@update')->name('admin.profile.update');

        //user
        Route::get('/user', 'backend\UserController@index')->name('admin.user');
        Route::get('/user/edit/{id}', 'backend\UserController@edit')->name('admin.edit');
        Route::post('/user/update/{id}', 'backend\UserController@update')->name('admin.update');
        Route::get('/user/create', 'backend\UserController@create')->name('admin.create');
        Route::get('/user/delete', 'backend\UserController@delete')->name('admin.delete');

        //datatable
        Route::post('/bulk/insert', 'backend\DatatableController@insert')->name('admin.datatable.insert');
        Route::get('/bulk/getDetail/{id}/{tbl}', 'backend\DatatableController@edit')->name('admin.datatable.edit');
        Route::post('/bulk/update', 'backend\DatatableController@update')->name('admin.datatable.update');
        Route::get('/bulk/delete', 'backend\DatatableController@delete')->name('admin.datatable.delete');
        Route::get('/bulk/deleteAll', 'backend\DatatableController@deleteAll')->name('admin.datatable.deleteAll');


         


        //category
        Route::get('/category', 'backend\CategoryController@index')->name('admin.category');

        //payment method
        Route::get('/payment-method', 'backend\PaymentMethodController@index')->name('admin.payment_method');

        // videos
        Route::get('/video', 'backend\VideoController@index')->name('admin.video');

        // banner
        Route::get('/banner', 'backend\BannerController@index')->name('admin.banner');

        // product
        Route::get('/product', 'backend\ProductController@index')->name('admin.product');

        // sale
        Route::get('/product/sale', 'backend\SaleController@index')->name('product.sale');
        Route::post('/product/sale/insert', 'backend\SaleController@insert')->name('product.sale.insert');

        // print invoice
        Route::get('/print/{id}', 'backend\SaleController@print')->name('product.sale.print');

        // invoice
        Route::get('/product/invoice', 'backend\InvoiceController@index')->name('product.invoice');
        Route::get('/product/invoice/detail/{id}', 'backend\InvoiceController@detail')->name('product.invoice.detail');


        // invoice report
        Route::get('/invoice-report', 'backend\ReportController@invoice')->name('invoice-report');
        Route::post('/invoice-report', 'backend\ReportController@invoice')->name('invoice-report.fillter');

        // sale report
        Route::get('/sale-report', 'backend\ReportController@sale')->name('sale-report');
        Route::post('/sale-report', 'backend\ReportController@sale')->name('sale-report.fillter');
        

        // permission
        Route::get('/permission', 'backend\PermissionController@index')->name('admin.permission');
        Route::post('/permission-insert', 'backend\PermissionController@insert')->name('admin.permission.insert');
        Route::get('/permission-delete', 'backend\PermissionController@delete')->name('admin.permission.delete');

        

        // roles
        Route::get('/role', 'backend\RoleController@index')->name('admin.role');
        Route::post('/role-insert', 'backend\RoleController@insert')->name('admin.role.insert');
        Route::get('/role-delete', 'backend\RoleController@delete')->name('admin.role.delete');


        // role permission
        Route::get('/role_permission/{role_id}', 'backend\RolePermissionController@index')->name('admin.role_permission');
        Route::get('/update-role-permission', 'backend\RolePermissionController@update')->name('admin.role_permission.update');
        Route::get('/regeneate-role-permission/{role_id}', 'backend\RolePermissionController@generate')->name('generatePermission');
        

        // 404
        Route::get('/404/page', function(){
            return view('backend.404.index');
        } )->name('404');
        

    });

    
});

// switch language
Route::get('/switch-lange/{lang}', 'LanguageController@switchLanguage')->name('app.lang');

//front end
Route::get('/', function(){
    return view('frontend.Home.index');
});


Route::fallback(function(){
    return redirect()->route('dashboard');
});




