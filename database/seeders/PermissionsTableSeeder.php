<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'product',
                'alias' => 'Product',
                'active' => 0,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 19:23:18',
                'updated_at' => '2023-01-16 19:37:43',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'product',
                'alias' => 'Product',
                'active' => 1,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 19:38:05',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 5,
                'name' => 'role',
                'alias' => 'Role',
                'active' => 1,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:04:17',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 6,
                'name' => 'user',
                'alias' => 'User',
                'active' => 1,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:04:27',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 7,
                'name' => 'company_info',
                'alias' => 'Company Info',
                'active' => 1,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:04:42',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 8,
                'name' => 'my_profile',
                'alias' => 'My Profile',
                'active' => 1,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:04:55',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}