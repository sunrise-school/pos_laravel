<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolePermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_permissions')->delete();
        
        \DB::table('role_permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 1,
                'permission_id' => 2,
                'list' => 1,
                'insert' => 1,
                'update' => 1,
                'delete' => 1,
                'active' => 1,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 19:52:04',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'role_id' => 1,
                'permission_id' => 5,
                'list' => 1,
                'insert' => 1,
                'update' => 1,
                'delete' => 1,
                'active' => 1,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:04:17',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'role_id' => 1,
                'permission_id' => 6,
                'list' => 1,
                'insert' => 1,
                'update' => 1,
                'delete' => 1,
                'active' => 1,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:04:27',
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'role_id' => 1,
                'permission_id' => 7,
                'list' => 1,
                'insert' => 1,
                'update' => 1,
                'delete' => 1,
                'active' => 1,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:04:42',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'role_id' => 1,
                'permission_id' => 8,
                'list' => 1,
                'insert' => 1,
                'update' => 1,
                'delete' => 1,
                'active' => 1,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:04:55',
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'role_id' => 2,
                'permission_id' => 2,
                'list' => 1,
                'insert' => 1,
                'update' => 1,
                'delete' => 1,
                'active' => 0,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:05:28',
                'updated_at' => '2023-01-16 20:17:56',
            ),
            6 => 
            array (
                'id' => 7,
                'role_id' => 2,
                'permission_id' => 5,
                'list' => 1,
                'insert' => 1,
                'update' => 1,
                'delete' => 1,
                'active' => 0,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:05:28',
                'updated_at' => '2023-01-16 20:17:56',
            ),
            7 => 
            array (
                'id' => 8,
                'role_id' => 2,
                'permission_id' => 6,
                'list' => 1,
                'insert' => 1,
                'update' => 1,
                'delete' => 1,
                'active' => 0,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:05:28',
                'updated_at' => '2023-01-16 20:17:56',
            ),
            8 => 
            array (
                'id' => 9,
                'role_id' => 2,
                'permission_id' => 7,
                'list' => 1,
                'insert' => 1,
                'update' => 1,
                'delete' => 1,
                'active' => 0,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:05:28',
                'updated_at' => '2023-01-16 20:17:56',
            ),
            9 => 
            array (
                'id' => 10,
                'role_id' => 2,
                'permission_id' => 8,
                'list' => 1,
                'insert' => 1,
                'update' => 1,
                'delete' => 1,
                'active' => 0,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:05:28',
                'updated_at' => '2023-01-16 20:17:56',
            ),
        ));
        
        
    }
}