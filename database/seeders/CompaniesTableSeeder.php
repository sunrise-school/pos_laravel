<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('companies')->delete();
        
        \DB::table('companies')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Coffee Shop',
                'photo' => 'image/gLxQkp5gsrk6wR9QLepIOAD7jE2z7dntM0cCk6ns.jpg',
                'address' => 'PhnomPenh',
                'phone' => '016855002',
                'active' => 1,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-04 19:15:13',
                'updated_at' => '2023-01-04 19:42:16',
            ),
        ));
        
        
    }
}