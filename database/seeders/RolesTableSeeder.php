<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Super User',
                'active' => 1,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 19:52:04',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'HR ADMIN',
                'active' => 0,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:05:28',
                'updated_at' => '2023-01-16 20:17:56',
            ),
        ));
        
        
    }
}