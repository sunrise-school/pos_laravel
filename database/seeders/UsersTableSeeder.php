<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 1,
                'name' => 'admin',
                'username' => 'admin',
                'password' => '12345678',
                'email' => 'g@gmail.com',
                'email_verified_at' => NULL,
                'remember_token' => NULL,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:01:18',
                'updated_at' => '2023-01-16 20:12:47',
                'active' => 1,
                'photo' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'role_id' => 1,
                'name' => 'kokleng',
                'username' => 'admin2',
                'password' => '123',
                'email' => '1673874114@gamil.com',
                'email_verified_at' => NULL,
                'remember_token' => NULL,
                'created_by' => NULL,
                'updated_by' => NULL,
                'created_at' => '2023-01-16 20:01:54',
                'updated_at' => '2023-01-16 20:18:12',
                'active' => 1,
                'photo' => NULL,
            ),
        ));
        
        
    }
}